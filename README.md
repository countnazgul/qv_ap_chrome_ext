# README #

Chrome extension the will easy and quick way to open Qlikview document from any Qlikview Access Point. 

The extension will show the same set of documents as the original Access Point.


Thanks to:

* @PeterJosling - for Options page design 

* @awbush - for jQuery Fast Live Filter plugin

Change Log:

v0.8 (2014-09-25)

* Can be searched and by document category

v0.7 (2014-09-25)

* Fixed login issie. Will work with both authentication methods

v0.6 (2014-09-25)

* Code clean up
* Error handling

v0.5 (2014-09-25)

* Options page is redesigned

v0.4 (2014-09-24)

* Initial Release